<?php
//require_once 'DbConnections.php';

class Email
{

    private $_db;
    private $_myragonDb;

    public function __construct(){
        $this->_db = DbConnections::getInstanceOfLandingDb();
        $this->_myragonDb = DbConnections::getInstanceOfMyragonDb();
    }

    public function getEmailsNotReminded(){
        /*
        $sql = "
            select t.email, t.app_data from (
                select email, app_data from form_steps_log
                where email is not null and email != '' and date > NOW() - interval 30 day #and date < NOW() - interval 30 minute
                group by email
            )t
            LEFT JOIN stat_sent_forms_tcsbank_universal ssftu ON ssftu.email = t.email AND ssftu.date > NOW() - interval 30 day
            LEFT JOIN email_remind er ON er.email = t.email
			WHERE ssftu.email is null AND (er.email is null OR er.last_update < NOW() - interval 30 day)
        ";
        */

        $sql = "
             select t.email, t.app_data from (
				select * from (
                select email, app_data from form_steps_log
                where email is not null and email != '' and date > NOW() - interval 30 day and date < NOW() - interval 300 minute
				order by email,step_id desc
				)u
                group by email
            )t
            LEFT JOIN stat_sent_forms_tcsbank_universal ssftu ON ssftu.email = t.email AND ssftu.date > NOW() - interval 30 day
            LEFT JOIN email_remind er ON er.email = t.email
			WHERE ssftu.email is null AND (er.email is null OR er.date_sent < NOW() - interval 30 day)
        ";

        $sth = $this->_db->prepare($sql);
        $sth->execute();

        $res = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }

    /*
     * Отправка напоминаний по крону
     */
    public function sendRemindEmails(){
        $data = $this->getEmailsNotReminded();

        foreach($data as $requestInfo){
            echo $requestInfo['email'];
            echo "\n <br>";
            //mail('vbuchin@myragon.ru', 'sendRemindEmails', 'msg');
            $this->updateEmailRemindTable($requestInfo['email']);
        }
    }

    public function updateEmailRemindTable($email){
        $sql = "
            INSERT INTO email_remind (email, date_sent) VALUES(?, NOW())
            ON DUPLICATE KEY UPDATE date_sent = NOW()
        ";
        $sth = $this->_db->prepare($sql);
        $sth->execute(array($email));
    }
}