<?php
//require_once 'DbConnections.php';
//require_once 'SendToAll.php';

class Queue extends BanksAbstract{

    private $_db;
    private $_myragonDb;

    public $aBanksInformationById = array();
    public $universalConfig = array();

    public function __construct(){
        parent::loadBanksConfig();
        $this->_db = DbConnections::getInstanceOfLandingDb();
        $this->_myragonDb = DbConnections::getInstanceOfMyragonDb();
    }

    public function addToQueue($bankId ,$data, $wId, $parentId){
        $data = json_encode($data);
        $sth = $this->_db->prepare('
        INSERT INTO queue (status, bank_id , user_id, app_data, date_add, parent_row_id)
        VALUES (1, :bankId, :wId, :appData, NOW(), :parentId)
        ');
        $sth->execute(array(
            ':wId' => $wId,
            ':bankId' => $bankId,
            ':appData' => $data,
            ':parentId' => $parentId

        ));
    }

    public function updateQueueStatus($id, $status){
        $sth = $this->_db->prepare('
        UPDATE queue SET status = :status, date_updated = NOW() WHERE id = :id
        ');
        $sth->execute(array(
            ':status' => $status,
            ':id' => $id
        ));
    }

    /*
     * Метод, забирающий заявки из очереди и рассылающий их
     */
    public function sendRequestsFromQueue(){
        $log = fopen('/srv/www/log/'.date('Y-m-d').'_QueueCron.log', 'a+');
        fwrite($log, date('Y-m-d H:i:s').' | ' . 'queue cron started' ."\n");

        $banksConfigs = $this->aBanksInformationById;

        foreach($banksConfigs as $bank){
            $queueData = $this->getQueueRequestsByBankId($bank['id']);

            if(!$bank['is_active']) continue;

            fwrite($log, date('Y-m-d H:i:s').' | ' . 'getQueueRequestsByBankId' ."\n");

            $queueCurrentLeads = 1;
            $queueLeadsLimit = $bank['leads_limit_queue_cron'];
            $bankId = $bank['id'];

            foreach($queueData as $k=>$request){
                fwrite($log, date('Y-m-d H:i:s').' | ' . 'request' ."\n");

                $parentRowId = $request['parent_row_id'];
                //временная затычка, в теории такого не должно быть
                if(!$parentRowId){
                    $parentRowId = 9999;
                }

                if($queueCurrentLeads >= $queueLeadsLimit){
                    fwrite($log, date('Y-m-d H:i:s').' | ' . 'leads limit for bank reached ' ."\n");
                    break;
                }

                $aggregationRow = $this->getRequestAggregationRow($parentRowId);

                $SendToAll = new SendToAll($aggregationRow['user_id'], $aggregationRow['sa']);
                $SendToAll->prepareDataFromAggregation($aggregationRow, 1,$bankId);


                $result = $SendToAll->sendToById($parentRowId, $bankId, 1);

                //print_r($result);
                //continue;

                if($result['result']){

                    fwrite($log, date('Y-m-d H:i:s').' | ' . 'hit queueId=' . $request['id'] ."\n");
                    if($result['send_result']['success']){
                        $queueCurrentLeads++;
                    }
                    $this->updateQueueStatus($request['id'], 2);
                }else{
                    fwrite($log, date('Y-m-d H:i:s').' | ' . 'today limit reached for targetId=' );
                    break;
                }
            }
        }

        fwrite($log, date('Y-m-d H:i:s').' | ' . 'queue cron closed' ."\n");
        fclose($log);
    }

    private function getRequestAggregationRow($id){
        $sht = $this->_db->prepare("
            SELECT * FROM stat_sent_forms_aggregation WHERE id = :id
        ");
        $sht->execute(array(':id' => $id));
        $res = $sht->fetch(PDO::FETCH_ASSOC);
        return $res;
    }

    private function getQueueRequestsByBankId($bankId){
        //$sth = $this->_db->prepare("SELECT * FROM queue WHERE bank_id = :bankId AND status = 1 order by date_add desc LIMIT 70");
        $sth = $this->_db->prepare("SELECT * FROM queue WHERE bank_id = :bankId AND status = 1 AND date_add > NOW() - interval 2 week order by date_add");
        $sth->execute(array(
            ':bankId' => $bankId
        ));
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}