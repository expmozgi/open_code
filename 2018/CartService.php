<?php


    /**
     * CartService
     * @package    Uteka
     * @subpackage API
     * @author     vbuchin
     */
    class CartService {

        use TApiService;


        /**
         * Get
         * @param int    $cityId
         * @param string $cartId
         * @param array  $pharmacyIds
         * @param int    $deliveryType 1 - all (new), 2 pickup only (old), 3 delivery only, 4 extended pickup
         * @return ApiUserCartResponse
         * @throws Exception
         */
        public function Get( $cityId, $cartId = null, $pharmacyIds = [], $deliveryType = OrderUtility::DeliveryTypePickUp ) {

            $city         = $this->city( $cityId );
            $siteUserCart = $this->siteUserCart( $city, $cartId );
            $pharmacyIds  = $this->filterPharmacyIds( $pharmacyIds );

            SiteUserCartManager::FillMissingProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );
            SiteUserCartManager::FillCartWithProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );

            $apiUserCart         = ApiObjectConverter::ConvertSiteUserCart( $siteUserCart );
            $apiUserCart->result = true;

            return $apiUserCart;
        }


        /**
         * Add
         * @param  int                $cityId
         * @param  ApiCartRequestItem $product
         * @param string              $cartId
         * @param array               $pharmacyIds
         * @param int                 $deliveryType 1 - all (new), 2 pickup only (old), 3 delivery only, 4 extended pickup
         * @return ApiUserCartResponse
         * @throws Exception
         */
        public function Add( $cityId, $product, $cartId = null, $pharmacyIds = [], $deliveryType = OrderUtility::DeliveryTypePickUp ) {
            return $this->AddBulk( $cityId, [ $product ], $cartId, $pharmacyIds, $deliveryType );
        }


        /**
         * Add Bulk
         * @param int                  $cityId
         * @param ApiCartRequestItem[] $products
         * @param string               $cartId
         * @param array                $pharmacyIds
         * @param int                  $deliveryType 1 - all (new), 2 pickup only (old), 3 delivery only, 4 extended pickup
         * @return ApiUserCartResponse
         * @throws Exception
         */
        public function AddBulk( $cityId, $products, $cartId = null, $pharmacyIds = [], $deliveryType = OrderUtility::DeliveryTypePickUp ) {
            $city         = $this->city( $cityId );
            $siteUserCart = $this->siteUserCart( $city, $cartId );
            $pharmacyIds  = $this->filterPharmacyIds( $pharmacyIds );

            if ( !$siteUserCart ) {
                throw new Exception( 'cart was not found', 404 );
            }

            SiteUserCartManager::FillMissingProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );

            $result = false;
            foreach ( $products as $product ) {
                $result = SiteUserCartManager::Add( $product, $siteUserCart, $pharmacyIds, $deliveryType );
                if ( !$result ) {
                    break;
                }
            }

            SiteUserCartManager::FillCartWithProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );

            $apiUserCart         = ApiObjectConverter::ConvertSiteUserCart( $siteUserCart );
            $apiUserCart->result = $result;

            return $apiUserCart;
        }


        /**
         * Remove
         *
         * @param  ApiCartRequestItem $product
         * @param string              $cartId
         * @param int                 $cityId
         * @param array               $pharmacyIds
         * @param int                 $deliveryType 1 - all (new), 2 pickup only (old), 3 delivery only, 4 extended pickup
         * @return ApiUserCartResponse
         * @throws Exception
         */
        public function Remove( $product, $cartId, $cityId = SiteUserManager::DefaultCityId, $pharmacyIds = [], $deliveryType = OrderUtility::DeliveryTypePickUp ) {
            $city         = $this->city( $cityId );
            $siteUserCart = $this->siteUserCart( null, $cartId );
            $pharmacyIds  = $this->filterPharmacyIds( $pharmacyIds );

            if ( !$siteUserCart ) {
                throw new Exception( 'cart was not found', 404 );
            }

            SiteUserCartManager::FillMissingProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );

            $result = SiteUserCartManager::Remove( $product, $siteUserCart );

            SiteUserCartManager::FillCartWithProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );

            $apiUserCart         = ApiObjectConverter::ConvertSiteUserCart( $siteUserCart );
            $apiUserCart->result = $result;

            return $apiUserCart;
        }


        /**
         * Refresh
         *
         * @param int    $cityId
         * @param string $cartId
         * @param array  $pharmacyIds
         * @param int    $deliveryType
         * @return ApiUserCartResponse
         * @throws Exception
         * @internal param ApiCartRequestItem $product
         */
        public function Refresh( $cityId, $cartId = null, $pharmacyIds = [], $deliveryType = OrderUtility::DeliveryTypePickUp ) {
            $city         = $this->city( $cityId );
            $siteUserCart = $this->siteUserCart( $city, $cartId );
            $pharmacyIds  = $this->filterPharmacyIds( $pharmacyIds );

            if ( !$siteUserCart ) {
                throw new Exception( 'cart was not found', 404 );
            }

            $result = SiteUserCartManager::Refresh( $siteUserCart, $city, $pharmacyIds );
            SiteUserCartManager::FillCartWithProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );

            $apiUserCart         = ApiObjectConverter::ConvertSiteUserCart( $siteUserCart );
            $apiUserCart->result = $result;

            return $apiUserCart;
        }


        /**
         * Destroy
         *
         * @param string $cartId
         * @return ApiUserCartResponse
         * @throws \Exception
         */
        public function Destroy( $cartId ) {
            $siteUserCart = $this->siteUserCart( null, $cartId );

            if ( !$siteUserCart ) {
                throw new Exception( 'cart was not found', 404 );
            }

            $result = SiteUserCartManager::Destroy( $siteUserCart );

            $apiUserCart         = new ApiUserCartResponse();
            $apiUserCart->result = $result;

            return $apiUserCart;
        }


    }