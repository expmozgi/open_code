<?php

    use Eaze\Core\DateTimeWrapper;


    /**
     * SiteUserCart
     *
     * @package    Uteka
     * @subpackage Orders
     */
    class SiteUserCart {

        /** @var string */
        public $siteUserCartId;

        /** @var int */
        public $statusId;

        /** @var Status */
        public $status;

        /** @var int */
        public $siteUserId;

        /** @var array */
        public $cart;

        /** @var string */
        public $createdAt;

        /** @var string */
        public $updatedAt;

        /** @var int */
        public $cityId;

        # user defined code goes below

        const cartCount = 'count';

        const cartProductId = 'productId';

        const cartSorting = 'sorting';

        const cartPrice = 'price';

        const cartExternalId = 'externalId';

        const cartPartNumber = 'partNumber';

        /** @var array */
        public $missingProductsArray;

        /** @var Product[] */
        public $missingProducts;

        /** @var Product[] */
        public $cartProducts;

        /** @var Product[] */
        public $cartProductsPickUp;

        /** @var Product[] */
        public $cartProductsDelivery;

        /**
         * @param $product
         * @return bool
         */
        public function AddToCart( $product ) {
            $cart = $this->GetCart();

            if ( empty( $product->productId ) || empty( $product->count ) ) {
                return false;
            }

            $product->count = abs( $product->count );

            if ( isset( $cart[$product->productId] ) ) {
                $cart[$product->productId][self::cartCount] += $product->count;
            } else {
                $product->sorting          = ( new DateTimeWrapper( null ) )->getTimestamp();
                $cart[$product->productId] = (array) $product;
            }

            $this->SetCart( $cart );

            return true;
        }


        /**
         * @param $product
         * @return bool
         */
        public function RemoveFromCart( $product ) {
            $cart = $this->GetCart();

            if ( isset( $product->productId, $cart[$product->productId] ) ) {
                $currentCount = $cart[$product->productId][self::cartCount];

                $toDeleteCount = $product->count ?? $currentCount;

                if ( $currentCount - $toDeleteCount <= 0 ) {
                    unset( $cart[$product->productId] );
                } else {
                    $cart[$product->productId][self::cartCount] = $currentCount - $toDeleteCount;
                }
            }

            $this->cart = $cart;

            return true;
        }


        /**
         * @return array
         */
        public function GetCart() {
            $cart = $this->cart ?? [];
            self::SortCart( $cart );

            return $cart;
        }


        /**
         * @param $cart
         */
        public function SetCart( $cart ) {
            $this->cart = $cart;
        }


        /**
         * @param $cart
         */
        public static function SortCart( array &$cart ) {
            uasort( $cart, function ( $a, $b ) {
                return ( $b[self::cartSorting] ?? 0 ) <=> ( $a[self::cartSorting] ?? 0 );
            } );
        }


        /**
         * @param array $cartEmptySorting
         * @param array $cartWithSorting
         */
        public static function FillCartSorting (array &$cartEmptySorting, array $cartWithSorting) {
            foreach ($cartEmptySorting as &$item) {
                $item['sorting'] = $cartWithSorting[$item[self::cartProductId]][self::cartSorting] ?? 0;
            }
        }


        /**
         * @return bool
         */
        public function IsCartEmpty() {
            $cart = $this->GetCart();

            if ( count( $cart ) > 0 ) {
                return false;
            }

            return true;
        }


        /**
         * Returns count of specified product in current cart
         *
         * @param int $productId
         * @return int
         */
        public function CountProductInCart( int $productId ): int {
            if ( array_key_exists( $productId, $this->GetCart() ) ) {
                return $this->GetCart()[$productId][self::cartCount];
            }

            return 0;
        }

        /**
         * @return ApiDeliveryTypeList|null
         */
        public function GetMinDeliveryCost() {
            return WerDeliveryData::DeliveryTypes( Context::$City->cityId, ceil( $this->CalculateCartValue($this->cartProductsDelivery) ), Context::$City );
        }

        /**
         * @param $cart
         * @return int
         */
        public function CalculateCartValue($cart) {
            $value = 0;
            if (is_array($cart)) {
                foreach ($cart as $item) {
                    $value += $item['minPrice'] ?? 0;
                }
            }

            return $value;
        }


        /**
         * CreateFromArray
         *
         * @param $cart
         * @param City $city
         * @param array $pharmacyIds
         * @param int $deliveryType
         * @return SiteUserCart
         */
        public static function CreateFromArray($cart, City $city, array $pharmacyIds, $deliveryType = OrderUtility::DefaultDeliveryType) {
            $siteUserCart = new self();
            $siteUserCart->cart = $cart;
            $siteUserCart->createdAt = \Eaze\Core\DateTimeWrapper::Now();
            $siteUserCart->cityId = Context::$City->cityId;

            SiteUserCartManager::FillCartWithProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );

            return $siteUserCart;
        }
    }
