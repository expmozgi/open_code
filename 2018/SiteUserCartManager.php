<?php

    use Eaze\Database\ConnectionFactory;
    use Eaze\Database\SqlCommand;
    use Eaze\Model\BaseFactory;
    use Eaze\Model\BaseFactoryPrepare;


    /**
     * SiteUserManager
     * @package    Uteka
     * @subpackage Orders
     * @author     vbuchin
     */
    class SiteUserCartManager {


        /**
         * @param              $product
         * @param siteUserCart $siteUserCart
         * @param array        $pharmacyIds
         * @param              $deliveryType
         * @return bool
         */
        public static function Add( $product, siteUserCart $siteUserCart, array $pharmacyIds, $deliveryType = OrderUtility::DefaultDeliveryType ) {
            $siteUserCartModified = clone ( $siteUserCart );

            $result = $siteUserCartModified->AddToCart( $product );

            if ( $result && self::isCartFine( $siteUserCartModified, $pharmacyIds, $deliveryType ) ) {
                SiteUserCartFactory::Update( $siteUserCartModified );
                $siteUserCart->updatedAt = \Eaze\Core\DateTimeWrapper::Now();
                $siteUserCart->cart      = $siteUserCartModified->cart;

                return true;
            }

            return false;
        }


        /**
         * @param              $product
         * @param siteUserCart $siteUserCart
         * @return bool
         */
        public static function Remove( $product, siteUserCart $siteUserCart ) {

            $result = $siteUserCart->RemoveFromCart( $product );

            if ( $result ) {
                $siteUserCart->updatedAt = \Eaze\Core\DateTimeWrapper::Now();
                SiteUserCartFactory::Update( $siteUserCart );
                return true;
            }

            return false;
        }


        /**
         * @param siteUserCart $siteUserCart
         * @param City         $city
         * @param array        $pharmacyIds
         * @param int          $deliveryType 1 - all (new), 2 pickup only (old), 3 delivery only, 4 extended pickup
         * @return bool
         */
        public static function Refresh( siteUserCart $siteUserCart, City $city, array $pharmacyIds, $deliveryType = OrderUtility::DefaultDeliveryType ) {
            return self::FillMissingProducts( $siteUserCart, $city, $pharmacyIds, $deliveryType );
        }


        /**
         * @param siteUserCart $siteUserCart
         * @return bool
         */
        public static function Destroy( siteUserCart $siteUserCart ) {
            return SiteUserCartFactory::Delete( $siteUserCart );
        }


        /**
         * @param siteUser $siteUser
         * @return bool
         */
        public static function DestroyAll( siteUser $siteUser ) {

            $sql = <<<SQL
UPDATE "siteUserCarts" SET "statusId" = @statusDeleted WHERE "statusId" = @statusNew AND "siteUserId" = @siteUserId
SQL;

            $cmd = new SqlCommand( $sql, ConnectionFactory::Get() );
            $cmd->SetInt( '@statusNew', StatusUtility::Enabled );
            $cmd->SetInt( '@statusDeleted', StatusUtility::Deleted );
            $cmd->SetInt( '@siteUserId', (int) $siteUser->siteUserId );
            $cmd->Execute();

            return true;
        }


        /**
         * Getting cart or creating one
         *
         * @param City     $city
         * @param int      $cartId
         * @param SiteUser $siteUser
         * @return SiteUserCart
         * @throws Exception
         */
        public static function Get( City $city = null, $cartId, SiteUser $siteUser = null ) {
            if ( $cartId ) {
                $suCart = self::GetById( $cartId );

                if ( !$suCart ) {
                    throw new Exception( "Cart is no longer available" );
                }

                $wasModified = false;
                $cityId      = $city->cityId ?? null;
                if ( $cityId && $suCart->cityId !== $cityId ) {
                    $suCart->cityId = $cityId;
                    $wasModified    = true;
                }

                $siteUserId = $siteUser->siteUserId ?? null;
                if ( $siteUserId && $suCart->siteUserId !== $siteUserId ) {
                    $suCart->siteUserId = $siteUserId;
                    $wasModified        = true;
                }

                if ( $wasModified ) {
                    SiteUserCartFactory::Update( $suCart );
                }

                return $suCart;
            }

            //Getting last cart from database
            if ( $siteUser ) {
                $search  = [ 'siteUserId' => $siteUser->siteUserId ];
                $options = [ BaseFactory::OrderBy => [ [ 'name' => 'updatedAt', 'sort' => 'DESC' ] ] ];

                $suCart = null;
                if ( $suCarts = SiteUserCartFactory::Get( $search, array_merge( $options, [ BaseFactory::WithoutPages => true ] ) ) ) {
                    $suCart = reset( $suCarts );
                }

                if ( $suCart ) {
                    return $suCart;
                }
            }

            //Creating new
            $suCart             = new SiteUserCart();
            $suCart->updatedAt  = \Eaze\Core\DateTimeWrapper::Now();
            $suCart->createdAt  = \Eaze\Core\DateTimeWrapper::Now();
            $suCart->statusId   = StatusUtility::Enabled;
            $suCart->siteUserId = $siteUser->siteUserId ?? null;
            $suCart->cityId     = $city->cityId ?? null;

            if ( !SiteUserCartFactory::Add( $suCart, [ BaseFactory::WithReturningKeys => true ] ) ) {
                throw new Exception( "Impossible to create cart" );
            }

            return $suCart;
        }


        /**
         * @param $cartId
         * @return SiteUserCart
         */
        public static function GetById( $cartId ) {
            $options[BaseFactory::CustomSql] = <<<SQL
 AND "updatedAt" >= (now() - INTERVAL '1 day')
SQL;
            //@TODO temp off cart expiring
            $options = [];

            return SiteUserCartFactory::GetById( $cartId, [], $options );
        }


        /**
         * isCartFine
         *
         * If cart exists at least in one pharmacy it will return true
         *
         * @param $userCart
         * @return bool
         */
        public static function isCartFine( siteUserCart $userCart, array $pharmacyIds, $deliveryType = OrderUtility::DefaultDeliveryType ) {

            $res = OrderUtility::FindPharmacies( array_values( $userCart->cart ), $userCart->cityId, $pharmacyIds, $deliveryType );

            if ( count( $res ) > 0 ) {
                return true;
            }

            return false;
        }


        /**
         * FillMissingProducts
         *
         * @param SiteUserCart $userCart
         * @param City         $city
         * @param array        $pharmacyIds
         * @param int          $deliveryType 1 - all (new), 2 pickup only (old), 3 delivery only, 4 extended pickup
         * @return bool
         */
        public static function FillMissingProducts( SiteUserCart $userCart, City $city, array $pharmacyIds, $deliveryType = OrderUtility::DefaultDeliveryType ) {
            if ( $userCart->IsCartEmpty() ) {
                return true;
            }
            $userCart->missingProductsArray = OrderUtility::FindMissingItems( array_values( $userCart->cart ?? [] ), $userCart->cityId, $pharmacyIds, $deliveryType );

            $productIds = [];
            $quantities = [];

            foreach ( $userCart->missingProductsArray as $item ) {
                if ( !empty( $item['productId'] ) ) {
                    $productIds[] = $item['productId'];
                    $quantities[$item['productId']] = $item['count'];
                }
            }

            if ( count( $productIds ) > 0 ) {
                $products = ProductFactory::Get( [ '_productId' => $productIds ], [ BaseFactory::WithoutPages => true ] );
                ProductManager::FillPrices( $products, $city->cityId, $pharmacyIds, $quantities );
                ProductManager::FillShort( $products );
                $userCart->missingProducts = $products;
            }

            return self::deleteMissingProducts( $userCart );
        }


        /**
         * DeleteMissingProducts
         *
         * @param SiteUserCart $userCart
         * @return bool
         */
        private static function deleteMissingProducts( SiteUserCart $userCart ) {
            if ( $userCart->missingProductsArray ) {
                foreach ( $userCart->missingProductsArray as $missingProduct ) {
                    $userCart->RemoveFromCart( (object) $missingProduct );
                }

                SiteUserCartFactory::Update( $userCart );

                return true;
            }

            return true;
        }


        /**
         * FillWithProducts
         *
         * @param SiteUserCart $userCart
         * @param City $city
         * @param array $pharmacyIds
         * @param int $deliveryType
         */
        public static function FillCartWithProducts( SiteUserCart $userCart, City $city, array $pharmacyIds, $deliveryType = OrderUtility::DeliveryTypePickUp ) {
            $productIds = [];
            $quantities = [];

            if ( $userCart->cart ) {
                foreach ( $userCart->cart as $item ) {
                    if ( !empty( $item['productId'] ) ) {
                        $productIds[] = $item['productId'];
                        $quantities[$item['productId']] = $item['count'];
                    }
                }
            }

            if ( count( $productIds ) > 0 ) {
                $products = ProductFactory::Get( [ '_productId' => $productIds ], [ BaseFactory::WithoutPages => true ] );
                ProductManager::FillPrices( $products, $city->cityId, $pharmacyIds, $quantities );
                ProductManager::FillShort( $products );
                $userCart->cartProducts = $products;

                $pickUpType           = $deliveryType === OrderUtility::DeliveryTypePickUp ? OrderUtility::DeliveryTypePickUp : OrderUtility::DeliveryTypeExtendedPickUp;
                $pickUpCombinations   = OrderUtility::FindOrderCombinations( array_values( $userCart->cart ?? [] ), $userCart->cityId, $pharmacyIds, $pickUpType );
                $deliveryCombinations = OrderUtility::FindOrderCombinations( array_values( $userCart->cart ?? [] ), $userCart->cityId, $pharmacyIds, OrderUtility::DeliveryTypeDelivery );

                self::FillMinAndMaxPrice($pickUpCombinations);
                self::FillMinAndMaxPrice($deliveryCombinations);

                $bestPickUpCombination = \Eaze\Helpers\ArrayHelper::GetFirstElement($pickUpCombinations);
                $bestDeliveryCombination = \Eaze\Helpers\ArrayHelper::GetFirstElement($deliveryCombinations);

                $bestPickUpCombination = !empty($bestPickUpCombination['info']) ? $bestPickUpCombination['info'] : [];
                $bestDeliveryCombination = !empty($bestDeliveryCombination['info']) ? $bestDeliveryCombination['info'] : [];

                $userCart->cartProductsPickUp = $bestPickUpCombination;
                $userCart->cartProductsDelivery = $bestDeliveryCombination;
            }
        }

        /**
         * FillMinAndMaxPrice based on combinations
         *
         * @param array $combinations
         */
        private static function FillMinAndMaxPrice (array &$combinations) {

            $products = [];

            foreach ($combinations as $combination) {
                foreach ($combination['info'] as $product) {
                    $products[$product['productId']][] = $product['price'];
                }
            }

            foreach ($products as $product) {
                sort($product);
            }

            foreach ($combinations as &$combination) {
                foreach ($combination['info'] as &$product) {
                    if (isset($products[$product['productId']])) {
                        $product['maxPrice'] = \Eaze\Helpers\ArrayHelper::GetFirstElement($products[$product['productId']]);
                        $product['minPrice'] = array_values(array_slice($products[$product['productId']], -1))[0];
                    }
                }

            }
        }
    }