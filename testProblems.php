<?php

/*Задание 1.

Пожалуйста, разработайте класс для "перемешивания" предложения.

Символ | является разделителем слов-вариантов. Например:

"{Пожалуйста|Просто} сделайте так, чтобы это {удивительное|крутое|простое} тестовое

предложение {изменялось {быстро|мгновенно} случайным образом|изменялось каждый

раз}."

На выходе должно получаться:

"Просто сделайте так, чтобы это удивительное тестовое предложение изменялось

каждый раз." или "Просто сделайте так, чтобы это удивительное тестовое предложение

изменялось мгновенно случайным образом".*/

class ReplaceWords
{

    private static  $_text = "{Пожалуйста|Просто} сделайте так, чтобы это {удивительное|крутое|простое} тестовое
предложение {изменялось {быстро|мгновенно} случайным образом|изменялось каждый
раз}.";

    private static  $_level1Regexp = "/\{(.*?)\}/s";
    private static  $_level2toNRegexp = "/\{.*\{(.*?)\}.*}/s";

    public static  function main ()
    {

//        print_r($this->_text);

        self::replace(self::$_level2toNRegexp);

//        echo "<br>";
//        print_r($this->_text);
//        echo "<br>";

        self::replace(self::$_level1Regexp);

        print_r(self::$_text);
    }

    private static function replace ($regexp)
    {
        preg_match($regexp, self::$_text, $result, PREG_OFFSET_CAPTURE);

//        print_r($result);

        if ($result[1]) {
            $exploded = explode('|', $result[1][0]);
            $word = $exploded[array_rand($exploded)];

            self::$_text = substr_replace(self::$_text, $word, $result[1][1] - 1, strlen($result[1][0]) + 2);

            self::replace($regexp);
        }
    }

}

//$ReplaceWords = new ReplaceWords();
//$ReplaceWords->main();

ReplaceWords::main();

/*Задание 2.

Необходио написать функцию для проверки корректности расположения скобочек в

строке.

Т.е. для каждой открывающей скобки должна быть своя закрывающая, и наоборот.

Также, скобочки не должны "пересекаться", т.е. строка '({)}' должна выявляться как

некорректная.*/

function checkBrackets ()
{
    //$text = '(rewerer){asd} ({asdasd}(sadasd)) ((({(error)}';
    //$text = '({({}{{}}(({{{({(error)} (rewerer){asd} ({asdasd}(sadasd)) ';
    $text = '({)}';

    $brackets = array ();
    $brackets['open'] = array('(', '{');
    $brackets['closed'] = array(')', '}');

    $stack = array();

    echo $text;
    for ($i = 0; $i<=strlen($text); $i++) {

        if(in_array($text[$i],$brackets['open'])) {
            $stack[] = array('text' => $text{$i}, 'position' => $i);
        }

        if (in_array($text[$i], $brackets['closed'])){
            $bracket = $brackets['open'][array_search($text[$i], $brackets['closed'])];
            if ( $stack[(count($stack)) - 1]['text'] == $bracket) {
                array_pop($stack);
            } else {
                throw new Exception('Недопустимая закрывающая скобка ' . $text[$i] . ' на позиции ' . $i );
            }
        }
    }

    if(count($stack) > 0) {
        $message = 'Отсутствуют закрывающие скобки для:';
        foreach($stack as $v) {
            $message .= ' Скобка ' . $v['text'] . ' на позиции ' . $v['position'] . "; ";
        }
        throw new Exception ($message);
    }

}

checkBrackets();