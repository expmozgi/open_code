<?php

class CampaignLinkChecker extends Model_Abstract
{
    private $_userAgents = array(
        0 => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.66 Safari/537.36',
        1 => 'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3',
        2 => 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
        3 => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.3',
        4 => 'Mozilla/5.0 (Windows NT 5.1; rv:25.0) Gecko/20100101 Firefox/25.0'
    );

    public function main($startHour = 9, $endHour = 2){
         $date = getdate();
         if($date['hours'] <= $startHour AND $date['hours'] >= $endHour) return false;
         $this->deleteOld();
         $this->checkLinks();

    }

    private function checkLinks(){
        $data = $this->getData();

        $alarmArray = array();

        foreach($data as $k=>&$v){
            echo "$k \n";
            //if($k > 200) break;
            $res = $this->openUrlViaCurl(trim($v['url']));
            $httpCode = $res['INFO']['http_code'];
            //echo $httpCode;die();

            if($httpCode == 301 || $httpCode == 302){
                $res = $this->openUrlViaCurl(trim($v['url']), true);
                $httpCode = $res['INFO']['http_code'];
            }

            if($httpCode != 200 && $httpCode != 301 && $httpCode != 302){
                echo 'founded';
                $v['http_code'] = $httpCode;
                $alarmArray[] = $v;
            }
        }

        $this->makeReport($alarmArray);
        $this->updateBadLinks($alarmArray);
    }

    private function deleteOld(){
        $query = "DELETE FROM myragon_cache.campaign_link_checker where last_checked < NOW() -interval 2 day";
        $this->db->query($query);
    }

    private function updateBadLinks($arr){
        foreach($arr as $v){
            $this->db->query("
  INSERT INTO myragon_cache.campaign_link_checker (campaign_id,campaign_url_id,campaing_url_geo_id, last_checked)
  VALUES ('{$v['campaign_id']}','{$v['cu_id']}','{$v['mcug_id']}', NOW())
  ON DUPLICATE KEY UPDATE last_checked = NOW();
  ");
        }
    }

    private function checkPreviousBadState($arr){
        $query = "
            SELECT * from myragon_cache.campaign_link_checker
            WHERE campaign_url_id = " . $arr['cu_id'] ."
            AND campaing_url_geo_id = ". $arr['mcug_id'] ."
            AND campaign_id = " . $arr['campaign_id'] . "
            AND last_checked > NOW() - interval 90 minute
        ";
        //print_r($query);
        $res = $this->db->fetchRow($query);
        return $res ? true : false;
    }

    private function makeReport($arr){
        //print_r($arr);
        if(!count($arr)) return false;
        //$resStr = 'Результаты: <br><br>';
        $resStr = '';

        $smsArr = array();

        foreach($arr as $k=>&$v){

            if($this->checkPreviousBadState($v)){
                $v['continious_error'] = true;
            }

            //if($v['is_work']){
            if($v['is_work'] AND $v['continious_error']){
                $smsArr[$v['campaign_id']] = array('name' => $v['c_name']);
            }

            if($k == 0){
                $resStr .= '<table border="1px">';
                $resStr .= '<tr>';
                $resStr .= '<td> campaign name </td>';
                $resStr .= '<td> campaign id </td>';
                $resStr .= '<td> campaign url id </td>';
                $resStr .= '<td> campaign url geo id </td>';
                $resStr .= '<td> http code </td>';
                $resStr .= '<td> is work </td>';
                $resStr .= '<td> url </td>';

                $resStr .= '</tr>';
            }

            if($v['is_work']){
                if($v['continious_error']){
                    $resStr .= '<tr style="background-color: #a31919;">';
                }else{
                    $resStr .= '<tr style="background-color: #EAF2D3;">';
                }
            }else{

                if($v['continious_error']){
                    $resStr .= '<tr style="background-color: #ecd1d1;">';
                }else{
                    $resStr .= '<tr>';
                }
            }

            $resStr .= '<td>'  . $v['c_name'] . '</td>';
            $resStr .= '<td>'  . $v['campaign_id'] . '</td>';
            $resStr .= '<td>'  . $v['cu_id'] . '</td>';
            $resStr .= '<td>'  . $v['mcug_id'] . '</td>';
            $resStr .= '<td>'  . $v['http_code'] . '</td>';
            $resStr .= '<td>'  . $v['is_work'] . '</td>';
            $resStr .= '<td><a href ="'  . $v['url'] . '">' . $v['url'] . '</a></td>';

            $resStr .= '</tr>';

        }
        $resStr .= '</table>';

        $this->sendEmails($resStr);

        if(count($smsArr)){
            $smsText = 'Обнаружены нерабочие ссылки рекламодателей ' .  "\n";
            foreach($smsArr as $k2=>$v2){
                $smsText .= $v2['name'] . '(' . $k2 . ')' . "\n";
            }

            $this->sendSms($smsText);
        }

        return true;

    }

    private function sendEmails($resStr){
        $emails = $this->getEmails();
        //$emails = array('vbuchin@myragon.ru');

        foreach($emails as $email){
            //echo $resStr;
            Tools::email(trim($email), 'Обнаружены нерабочие ссылки рекламодателей', $resStr);
        }
    }

    private function sendSms($smsText){
        $phones = $this->getPhones();
        //print_r($phones);die();
        if(!count($phones)) return false;
        foreach ($phones as $phone) {

            $smsText = LangTools::convertRusToLatStr($smsText, " ");
            //print $smsText;
            Tools::SendSMSMessage(trim($phone), $smsText);
        }
    }

    private function getEmails(){
        $res = $this->db->fetchRow("SELECT * FROM myragon._config WHERE code = 'monitor_campaign_links_check_email'");
        //print_r($res);
        $exploded = explode(',', $res['value']);
        return $exploded;
    }

    private function getPhones(){
        $res = $this->db->fetchRow("SELECT * FROM myragon._config WHERE code = 'monitor_campaign_links_check_sms'");
        //print_r($res);
        $exploded = explode(',', $res['value']);
        return $exploded;
    }

    private function getData(){
        $query = "
          SELECT  cug.id mcug_id ,cu.campaign_id, cug.url, cug.name, ct.is_active, cu.id cu_id, myragon.ret_lang_str(c.name_id) c_name
        , if(a.action_target_audience_id > 0, 1, 0) is_work

        FROM myragon.campaign_url cu
        INNER JOIN myragon.campaign_url_geo cug ON cug.campaign_url_id = cu.id

        INNER JOIN myragon.campaign c ON c.id = cu.campaign_id and c.is_active=1 and c.is_deleted=0
        INNER JOIN myragon.action_has_campaign ahc  ON ahc.campaign_id  = c.id
        INNER JOIN myragon.action a ON a.id  = ahc.action_id  and a.is_active=1 and a.is_deleted=0

        LEFT JOIN myragon.campaign_target ct  ON cu.campaign_target_id = ct.id
        WHERE cu.is_visible = 1 AND cug.is_active = 1 AND (ct.is_active is null or ct.is_active = 1 )
        AND cug.url != ''
        ";
        $items = $this->db->fetchAll($query);
        return $items;
    }

    public function openUrlViaCurl($url, $useCookie = false){
        $curl = curl_init();
        $ua = $this->_userAgents[array_rand($this->_userAgents)];
        //print $ua;
        curl_setopt($curl, CURLOPT_URL, $url);
        //curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/16.0.0.13');
        curl_setopt($curl,CURLOPT_USERAGENT, $ua);
        curl_setopt($curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($curl, CURLOPT_FAILONERROR, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 9);
        curl_setopt($curl, CURLOPT_HEADER, true);
        if($useCookie){
            $cookieIn = APPLICATION_PATH . '/../temp/CLCoucookie.txt';
            $cookieOut = APPLICATION_PATH . '/../temp/CLCincookie.txt';
            curl_setopt($curl, CURLOPT_COOKIEJAR,  $cookieIn);
            curl_setopt($curl, CURLOPT_COOKIEFILE, $cookieOut);
        }
        $html = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if($useCookie){
            try{
                unlink($cookieIn);
                unlink($cookieOut);
                sleep(1);
            }catch (Exception $e){
                echo $e->getMessage();
            }

        }

        return array("SOURCE" => $html, "INFO" => $info);
        //return array("INFO" => $info);
    }
}