//Авто голосовалка для topface

function doVote(){
    console.log('doVote()');
    var randomValue = Math.floor((Math.random() * 3) + 1);

    if (randomValue > 1) {
        voteSympathy();
    } else {
        voteSkip();
    }
}

function voteSympathy(){
    console.log('voteSympathy()');
    var randomValue = Math.floor((Math.random() * 9) + 1);

    setTimeout(function(){
        $('.dating-button-sympathy').click();
        startNewIterationCallback();
    }, randomValue * 1000);

}

function voteSkip(){
    console.log('voteSkip()');
    var randomValue = Math.floor((Math.random() * 9) + 1);

    setTimeout(function(){
        $('.dating-button-skip').click();
        startNewIterationCallback();
    }, randomValue * 1000);

}

function startNewIterationCallback() {
    console.log('startNewIterationCallback()');
    setTimeout(function(){
        doVote();
    }, 4000);
}

doVote();
